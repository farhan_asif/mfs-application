<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Home</a>
  <ul>
    <li class="active"><a href="index.html"><i class="icon icon-home"></i> <span>Home</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Fund Transfer</span></a>
      <ul>
        <li><a href="<c:url value='/getfund' />">Get Money</a></li>
        <li><a href="<c:url value='/sendfund' />">Send Money</a></li>
      </ul>
    </li>
    <%-- <li> <a href="<c:url value='/getfund' />"><i class="icon icon-money"></i> <span>Get Money</span></a> </li>
    <li> <a href="<c:url value='/sendfund' />"><i class="icon icon-inbox"></i> <span>Send Money</span></a> </li> --%>
    <li><a href="<c:url value='/accountlist' />"><i class="icon icon-th"></i> <span>Account List</span></a></li>
    <li><a href="<c:url value='/addnewbankaccount' />"><i class="icon icon-fullscreen"></i> <span>Add New Bank Account</span></a></li>
  </ul>
</div>
<!--sidebar-menu-->