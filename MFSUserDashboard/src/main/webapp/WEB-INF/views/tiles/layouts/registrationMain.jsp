<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="mfs"%>
 <html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><mfs:getAsString name="title" /></title>
    
	<link href="<c:url value='/static/css/bootstrap.min.css' />"  rel="stylesheet" />
	<link href="<c:url value='/static/css/bootstrap-responsive.min.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/css/matrix-style.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/css/matrix-media.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/font-awesome/css/font-awesome.css' />"  rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
        <header id="header">
            <mfs:insertAttribute name="header" />
        </header>
     
     	<!--sidebar-menu-->
        <!-- <section id="sidemenu"> -->
            <mfs:insertAttribute name="menu" />
        <!-- </section> -->
        <!--sidebar-menu-->
             
        <!--main-container-part-->
		<div id="content">
			
		
			<!--Action boxes-->
			  <div class="container-fluid">
				<!--End-Action boxes-->    
	            <mfs:insertAttribute name="body" />
              </div>
        </div>
         
        <footer id="footer">
            <mfs:insertAttribute name="footer" />
        </footer>


<script src="<c:url value='/static/js/jquery.min.js' />"></script> 
<script src="<c:url value='/static/js/bootstrap.min.js' />"></script> 
<script src="<c:url value='/static/js/custom.js'/>"></script>
</body>
</html>