<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="mfs"%>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    	<title><mfs:getAsString name="title" /></title>
		<link href="<c:url value='/static/css/bootstrap.min.css' />"  rel="stylesheet" />
		<link href="<c:url value='/static/css/bootstrap-responsive.min.css' />"  rel="stylesheet"/>
        <link rel="stylesheet" href="<c:url value='/static/css/matrix-login.css' />" />
        <link href="<c:url value='/static/font-awesome/css/font-awesome.css' />"  rel="stylesheet"/>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
<mfs:insertAttribute name="body" />
<script src="<c:url value='/static/js/jquery.min.js' />"></script>
        <script src="<c:url value='/static/js/matrix.login.js' />"></script>
    </body>

</html>