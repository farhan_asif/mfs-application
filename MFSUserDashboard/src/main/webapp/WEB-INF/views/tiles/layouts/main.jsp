<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="mfs"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 <html>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><mfs:getAsString name="title" /></title>
    
	<link href="<c:url value='/static/css/bootstrap.min.css' />"  rel="stylesheet" />
	<link href="<c:url value='/static/css/bootstrap-responsive.min.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/css/fullcalendar.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/css/matrix-style.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/css/matrix-media.css' />"  rel="stylesheet"/>
	<link href="<c:url value='/static/font-awesome/css/font-awesome.css' />"  rel="stylesheet"/>
	<link rel="stylesheet" href="<c:url value='/static/css/jquery.gritter.css' />"  rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>
        <header id="header">
            <mfs:insertAttribute name="header" />
        </header>
     
     	<!--sidebar-menu-->
        <!-- <section id="sidemenu"> -->
            <mfs:insertAttribute name="menu" />
        <!-- </section> -->
        <!--sidebar-menu-->
             
        <!--main-container-part-->
		<div id="content">
			<!--breadcrumbs-->
			  <div id="content-header">
			    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
			  </div>
			<!--End-breadcrumbs-->
		
			<!--Action boxes-->
			  <div class="container-fluid">
			    <div class="quick-actions_homepage">
			      <ul class="quick-actions">
			        <li class="bg_lb"> <a href="<c:url value='/' />"> <i class="icon-dashboard"></i> Home </a> </li>
			        <li class="bg_lg span3"> <a href="<c:url value='/getfund' />"> <i class="icon-signal"></i> Get Money</a> </li>
			        <li class="bg_ly"> <a href="<c:url value='/sendfund' />"> <i class="icon-inbox"></i> Send Money</a> </li>
			        <li class="bg_lo"> <a href="tables.html"> <i class="icon-th"></i> Tables</a> </li>
			        <li class="bg_ls"> <a href="grid.html"> <i class="icon-fullscreen"></i> Full width</a> </li>
			        <li class="bg_lo span3"> <a href="form-common.html"> <i class="icon-th-list"></i> Forms</a> </li>
			        <li class="bg_ls"> <a href="buttons.html"> <i class="icon-tint"></i> Buttons</a> </li>
			        <li class="bg_lb"> <a href="<c:url value='/passwordreset' />"> <i class="icon-pencil"></i>Reset Password</a> </li>
			        <li class="bg_lg"> <a href="calendar.html"> <i class="icon-calendar"></i> Calendar</a> </li>
			        <li class="bg_lr"> <a href="error404.html"> <i class="icon-info-sign"></i> Error</a> </li>
			
			      </ul>
			    </div>
				<!--End-Action boxes-->    
	            <mfs:insertAttribute name="body" />
              </div>
        </div>
         
        <footer id="footer">
            <mfs:insertAttribute name="footer" />
        </footer>

<script src="<c:url value='/static/js/excanvas.min.js' />"></script> 
<script src="<c:url value='/static/js/jquery.min.js' />"></script> 
<script src="<c:url value='/static/js/jquery.ui.custom.js' />"></script> 
<script src="<c:url value='/static/js/bootstrap.min.js' />"></script> 
<script src="<c:url value='/static/js/jquery.flot.min.js' />"></script> 
<script src="<c:url value='/static/js/jquery.flot.resize.min.js' />"></script> 
<script src="<c:url value='/static/js/jquery.peity.min.js' />"></script> 
<script src="<c:url value='/static/js/fullcalendar.min.js' />"></script> 
<script src="<c:url value='/static/js/matrix.js' />"></script> 
<script src="<c:url value='/static/js/matrix.dashboard.js' />"></script> 
<script src="<c:url value='/static/js/jquery.gritter.min.js' />"></script> 
<script src="<c:url value='/static/js/matrix.interface.js' />"></script> 
<script src="<c:url value='/static/js/matrix.chat.js' />"></script> 
<script src="<c:url value='/static/js/jquery.validate.js' />"></script> 
<script src="<c:url value='/static/js/matrix.form_validation.js' />"></script> 
<script src="<c:url value='/static/js/jquery.wizard.js' />"></script> 
<script src="<c:url value='/static/js/jquery.uniform.js' />"></script> 
<script src="<c:url value='/static/js/select2.min.js' />"></script> 
<script src="<c:url value='/static/js/matrix.popover.js' />"></script> 
<script src="<c:url value='/static/js/jquery.dataTables.min.js' />"></script> 
<script src="<c:url value='/static/js/matrix.tables.js' />"></script> 
<script src="<c:url value='/static/js/matrix.wizard.js'/>"></script>
<script src="<c:url value='/static/js/custom.js'/>"></script>

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>