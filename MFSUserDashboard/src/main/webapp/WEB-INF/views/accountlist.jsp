<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row-fluid">
<div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title" > <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>List Of Account</h5>
        </div>        
        <div class="widget-content nopadding">
			<table class="table table-hover">
	    		<thead>
		      		<tr>
				        <th>Account name</th>
				        <th>Mobile No</th>
				        <th>Bank</th>
				        <th>Branch</th>
				        <th>Account Number</th>
				        <th width="100"></th>
				        <th width="100"></th>
					</tr>
		    	</thead>
	    		<tbody>
				<c:forEach items="${accounts}" var="account">
					<tr>
						<td>${account.accountHolderName}</td>
						<td>${account.mobileNo}</td>
						<td>${account.bankName}</td>
						<td>${account.branchName}</td>
						<td>${account.accountNum}</td>
						<td><a href="#" class="btn btn-success custom-width">edit</a></td>
						<td><a href="#" class="btn btn-danger custom-width">delete</a></td>
						<%-- <td><a href="<c:url value='/edit-user-${user.ssoId}' />" class="btn btn-success custom-width">edit</a></td>
						<td><a href="<c:url value='/delete-user-${user.ssoId}' />" class="btn btn-danger custom-width">delete</a></td> --%>
					</tr>
				</c:forEach>
	    		</tbody>
	    	</table>
		</div>
	 	<%-- <div class="well">
	 		<a href="<c:url value='/accountopen' />">Add New User</a>
	 	</div> --%>
   	</div>
   	</div>
   	</div>
   	</div>