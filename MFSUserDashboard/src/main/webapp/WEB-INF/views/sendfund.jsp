<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="mfs"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row-fluid">
    
       <div class="span10">
        <div class="widget-box widget-chat">
          <div class="widget-title bg_lb"> <span class="icon"> <i class=" icon-briefcase"></i> </span>
            <h5>Fund Transfer</h5>
          </div>
          <div class="widget-content nopadding collapse in" id="collapseG4">
            <div class="chat-users panel-right2 span5">
              <div class="panel-title">
                <h5>Total Balance: 0</h5>
              </div>
              <div class="panel-content nopadding ">
                      <div class="control-group">
                        <div class="controls">
                          <c:forEach items="${accounts}" var="account">
                          <label>
                            <input type="radio" name="bank" value="${account.accountNum}" onClick="setBankInFundTransfer()" />
                            ${account.bankName}-${account.accountNum}</label>
                          </c:forEach>
                        </div>
                      </div>
              </div>
            </div>
            <div class="chat-content panel-left2" style="height: 250px; margin-left: 270px">
                  <form:form action="#" modelAttribute="sendFund" method="post" class="form-horizontal" name="fund_transfer_validate" id="fund_transfer_validate" novalidate="novalidate">
                    <%-- <form:input type="hidden" path="bankId" name="txtBankId" id="bankId" />
                    <form:input type="hidden" path="bankName" name="txtBankName" id="bankName" /> --%>
                    <form:input type="hidden" path="accountNum" name="txtAccountNum" id="accountNumber" />
                    
                    <div class="control-group">
                      <label class="control-label">Account NO :</label>
                      <div class="controls">
                        <form:input type="text" class="span6" path="toAccount" name="toAccountNumTxt" id="toAccountNum" placeholder="Enter account no" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Amount :</label>
                      <div class="controls">
                        <form:input type="number" class="span6" path="amount"  name="txtAmount" id="txtAmount" placeholder="Enter amount" />
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label">Pin :</label>
                      <div class="controls">
                        <form:input type="text" class="span6" path="pinPwd" name="txtPin" id="txtPin" placeholder="Enter pin" />
                      </div>
                    </div>
                    <div class="control-group">
	                    <div class="controls">
	                        <input class="btn btn-success" value="Transfer" type="submit">
	                    </div>
					</div>
                  </form:form>
             
            </div>
          </div>
        </div>

      </div> 

  </div>