<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="mfs"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container-fluid">
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
              <h5>Change Password</h5>
            </div>
            <div class="widget-content nopadding">
              <form:form class="form-horizontal" modelAttribute="account" method="post" name="password_validate" id="password_validate" novalidate="novalidate">
                <div class="control-group">
                  <label class="control-label">New Password</label>
                  <div class="controls">
                    <form:input type="password" path="webPassword" name="txtPass" id="txtPass" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Confirm password</label>
                  <div class="controls">
                    <input type="password" name="txtPass2" id="txtPass2" />
                  </div>
                </div>
                <div class="form-actions ">
                  <input type="submit" value="Change" class="btn btn-success">
                </div>
              </form:form>
            </div>
          </div>
        </div>
      </div>

  </div>