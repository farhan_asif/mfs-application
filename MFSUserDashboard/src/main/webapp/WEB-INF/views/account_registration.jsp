<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="mfs"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="row-fluid">

		<%-- <form:input type="hidden" path="id" id="id"/> --%>
<div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title" > <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Registration-info</h5>
        </div>        
        <div class="widget-content nopadding">
        <form:form method="POST" class="form-horizontal" action="saveaccount" modelAttribute="account">
            <div class="control-group">
              <label class="control-label">Name :</label>
              <div class="controls">
                <form:input type="text" class="span6" path="accountHolderName" name="txtName" id="txtName" placeholder="Enter name" />
              </div>
            </div>
            <!-- <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <input type="email" class="span6" path="" name="txtEmail" id="txtEmail" placeholder="Enter email" />
              </div>
            </div> -->
            <div class="control-group">
              <label class="control-label">Mobile No :</label>
              <div class="controls">
               <form:input type="text" maxlength="11"  class="span6" path="mobileNo" name="mobileNo" id="txtMobileNo" placeholder="Enter Mobile No"  />
              	<%-- <div class="has-error">
						<form:errors path="mobileNo" class="help-inline"/>
				</div> --%>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Bank Name :</label>
              <div class="controls" >
                <form:select class="span6" path="bankId" name="ddlBankId" id="ddlBank" onChange="getBankName()">
                <option value="0">--- Select Bank---</option>
                <c:forEach items="${banks}"  var="bank">
			         <form:option value="${bank.bankId}">${bank.bankName}</form:option>
			     </c:forEach>
                </form:select>
                <form:input type="hidden" path="bankName" name="txtBankName" id="txtBank" />
                <%-- <div class="has-error">
						<form:errors path="firstName" class="help-inline"/>
				</div> --%>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Branch Name :</label>
              <div class="controls">
                <form:select class="span6" path="branchId" name="ddlBranchId" id="ddlBranch" onChange="getBranchName()">
                <option value="0">--- Select Branch---</option>
                  <c:forEach items="${branchs}"  var="branch">
			         <form:option value="${branch.branchId}">${branch.branchName}</form:option>
			     </c:forEach>
                </form:select>
                <form:input type="hidden" path="branchName" name="txtBranchName" id="txtBranch" />
                <%-- <div class="has-error">
						<form:errors path="firstName" class="help-inline"/>
				</div> --%>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Branch office Name :</label>
              <div class="controls">
                <form:input type="text" class="span6" path="branchOfficeName" name="txtBranchOfficeName" id="txtBranchOfficeName" placeholder="Enter name" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Account Number :</label>
              <div class="controls">
                <form:input type="text" class="span6" path="accountNum" name="txtAccountNum" id="txtAccountNum" placeholder="Enter account no " />
              </div>
            </div>
            <div class="form-actions">
             <input type="submit" value="Submit" class="btn btn-primary btn-sm"/>
            </div>
          </form:form>
        </div>
      </div>
     
      
    </div>
    
  </div>

</div>