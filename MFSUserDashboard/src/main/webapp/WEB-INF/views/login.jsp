<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
        <div id="loginbox">      
        <c:url var="loginUrl" value="/login" />      
            <form:form id="loginform" action="${loginUrl}" method="post" modelAttribute="account" class="form-vertical">
				 <div class="control-group normal_text"> <h3><img src="<c:url value='/static/img/logo.png' />" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span>
                            <form:input type="text" id="webUser" path="webUser" placeholder="Username" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span>
                            <form:input type="password" id="webPassword" path="webPassword" placeholder="Password" />
                        </div>
                    </div>
                </div>
               <%--  <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" /> --%>
                <div class="form-actions">
                    <!-- <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span> -->
                    <span class="pull-left"><a href="<c:url value='/accountopen' />" class="flip-link btn btn-info">Create New Account</a></span>
                    <span class="pull-right">
                    <input type="submit" class="btn btn-success" value="Log in"></span>
                </div>
                
            </form:form>
            <%-- <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
				
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><a class="btn btn-info"/>Reecover</a></span>
                </div>
            </form> --%>
        </div>
        
        
