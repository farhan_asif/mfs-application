package com.entertech.mfsuser.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ACCOUNT_INFO database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_INFO")
@NamedQuery(name="AccountInfo.findAll", query="SELECT a FROM AccountInfo a")
public class AccountInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNT_INFO_IID_GENERATOR", sequenceName="ACCOUNT_INFO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_INFO_IID_GENERATOR")
	private long iid;

	@Temporal(TemporalType.DATE)
	@Column(name="MOBILE_LAST_ACCESS_DATE")
	private Date mobileLastAccessDate;

	@Temporal(TemporalType.DATE)
	@Column(name="MOBILE_LAST_PWD_CHANGE_DATE")
	private Date mobileLastPwdChangeDate;

	@Column(name="MOBILE_NO")
	private String mobileNo;

	@Column(name="MOBILE_PASSWORD")
	private String mobilePassword;

	@Temporal(TemporalType.DATE)
	@Column(name="OPEN_DATE")
	private Date openDate;

	@Temporal(TemporalType.DATE)
	@Column(name="WEB_LAST_ACCESS_DATE")
	private Date webLastAccessDate;

	@Temporal(TemporalType.DATE)
	@Column(name="WEB_LAST_PWD_CHANGE_DATE")
	private Date webLastPwdChangeDate;

	@Column(name="WEB_PASSWORD")
	private String webPassword;

	@Column(name="WEB_USER")
	private String webUser;

	//bi-directional many-to-one association to AccountDetail
	@OneToMany(mappedBy="accountInfo", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<AccountDetail> accountDetails;

	public AccountInfo() {
	}
	
	/*public AccountInfo(String mobileNo,String mobilePassword,String webPassword,String webUser,Set<AccountDetail> accountDetails ) {
		this.mobileNo=mobileNo;
		this.mobilePassword=mobilePassword;
		this.webPassword=webPassword;
		this.webUser=webUser;
		this.accountDetails=accountDetails;
	}*/

	public long getIid() {
		return this.iid;
	}

	public void setIid(long iid) {
		this.iid = iid;
	}

	public Date getMobileLastAccessDate() {
		return this.mobileLastAccessDate;
	}

	public void setMobileLastAccessDate(Date mobileLastAccessDate) {
		this.mobileLastAccessDate = mobileLastAccessDate;
	}

	public Date getMobileLastPwdChangeDate() {
		return this.mobileLastPwdChangeDate;
	}

	public void setMobileLastPwdChangeDate(Date mobileLastPwdChangeDate) {
		this.mobileLastPwdChangeDate = mobileLastPwdChangeDate;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMobilePassword() {
		return this.mobilePassword;
	}

	public void setMobilePassword(String mobilePassword) {
		this.mobilePassword = mobilePassword;
	}

	public Date getOpenDate() {
		return this.openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public Date getWebLastAccessDate() {
		return this.webLastAccessDate;
	}

	public void setWebLastAccessDate(Date webLastAccessDate) {
		this.webLastAccessDate = webLastAccessDate;
	}

	public Date getWebLastPwdChangeDate() {
		return this.webLastPwdChangeDate;
	}

	public void setWebLastPwdChangeDate(Date webLastPwdChangeDate) {
		this.webLastPwdChangeDate = webLastPwdChangeDate;
	}

	public String getWebPassword() {
		return this.webPassword;
	}

	public void setWebPassword(String webPassword) {
		this.webPassword = webPassword;
	}

	public String getWebUser() {
		return this.webUser;
	}

	public void setWebUser(String webUser) {
		this.webUser = webUser;
	}

	public List<AccountDetail> getAccountDetails() {
		return this.accountDetails;
	}

	public void setAccountDetails(List<AccountDetail> accountDetails) {
		this.accountDetails = accountDetails;
	}

	public AccountDetail addAccountDetail(AccountDetail accountDetail) {
		getAccountDetails().add(accountDetail);
		accountDetail.setAccountInfo(this);

		return accountDetail;
	}

	public AccountDetail removeAccountDetail(AccountDetail accountDetail) {
		getAccountDetails().remove(accountDetail);
		accountDetail.setAccountInfo(null);

		return accountDetail;
	}
	
	@Override
    public int hashCode() {
        return new Long(iid).hashCode();
    }
	
	 @Override
	    public boolean equals(Object obj) {
	        if (obj == null) {
	            return false;
	        }
	        if (!(obj instanceof AccountInfo)) {
	            return false;
	        }
	        return this.iid == ((AccountInfo) obj).getIid();
	    }

}