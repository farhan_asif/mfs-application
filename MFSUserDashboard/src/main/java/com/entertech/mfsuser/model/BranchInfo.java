package com.entertech.mfsuser.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the BRANCH_INFO database table.
 * 
 */
@Entity
@Table(name="BRANCH_INFO")
@NamedQuery(name="BranchInfo.findAll", query="SELECT b FROM BranchInfo b")
public class BranchInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BRANCH_INFO_BRANCHID_GENERATOR", sequenceName="BRANCH_INFO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BRANCH_INFO_BRANCHID_GENERATOR")
	@Column(name="BRANCH_ID")
	private long branchId;

	@Column(name="BANK_ID")
	private BigDecimal bankId;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="BRANCH_USER")
	private String branchUser;

	@Column(name="BRANCH_USER_PASS")
	private String branchUserPass;

	@Column(name="ENTERED_BY")
	private String enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATE")
	private Date enteredDate;

	public BranchInfo() {
	}

	public long getBranchId() {
		return this.branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public BigDecimal getBankId() {
		return this.bankId;
	}

	public void setBankId(BigDecimal bankId) {
		this.bankId = bankId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchUser() {
		return this.branchUser;
	}

	public void setBranchUser(String branchUser) {
		this.branchUser = branchUser;
	}

	public String getBranchUserPass() {
		return this.branchUserPass;
	}

	public void setBranchUserPass(String branchUserPass) {
		this.branchUserPass = branchUserPass;
	}

	public String getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDate() {
		return this.enteredDate;
	}

	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

}