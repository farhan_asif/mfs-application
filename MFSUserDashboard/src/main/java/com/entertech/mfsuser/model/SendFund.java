package com.entertech.mfsuser.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the SEND_FUND database table.
 * 
 */
@Entity
@Table(name="SEND_FUND")
@NamedQuery(name="SendFund.findAll", query="SELECT s FROM SendFund s")
public class SendFund implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ACCOUNT_NUM")
	private String accountNum;

	private double amount;

	@Column(name="BANK_ID")
	private BigDecimal bankId;

	@Column(name="BANK_NAME")
	private String bankName;

	@Column(name="BRANCH_ID")
	private BigDecimal branchId;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATE_DATE")
	private Date createDate;

	@Column(name="CREATE_TIME")
	private Timestamp createTime;

	@Temporal(TemporalType.DATE)
	@Column(name="DENIED_DATE")
	private Date deniedDate;

	@Column(name="DENIED_REASON")
	private String deniedReason;

	@Column(name="DENIED_TIME")
	private Timestamp deniedTime;

	@Id
	@SequenceGenerator(name="SEND_FUND_IID_GENERATOR", sequenceName="SEND_FUND_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEND_FUND_IID_GENERATOR")
	private BigDecimal iid;

	@Column(name="MOBILE_NO")
	private String mobileNo;

	@Column(name="PIN_PWD")
	private String pinPwd;

	private String reference;

	@Temporal(TemporalType.DATE)
	@Column(name="REQUEST_DATE")
	private Date requestDate;

	@Column(name="REQUEST_TIME")
	private Timestamp requestTime;

	private String security1;

	private String security2;

	private String security3;

	@Temporal(TemporalType.DATE)
	@Column(name="SEND_DATE")
	private Date sendDate;

	@Column(name="SEND_TIME")
	private Timestamp sendTime;

	@Column(name="TO_ACCOUNT")
	private String toAccount;

	@Column(name="TO_BANK")
	private BigDecimal toBank;

	@Column(name="TO_BRANCH")
	private BigDecimal toBranch;

	@Column(name="TRX_NUM")
	private String trxNum;

	@Column(name="TRX_NUM2")
	private String trxNum2;

	@Column(name="WEB_USER")
	private String webUser;

	public SendFund() {
	}

	public String getAccountNum() {
		return this.accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public BigDecimal getBankId() {
		return this.bankId;
	}

	public void setBankId(BigDecimal bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public BigDecimal getBranchId() {
		return this.branchId;
	}

	public void setBranchId(BigDecimal branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Date getDeniedDate() {
		return this.deniedDate;
	}

	public void setDeniedDate(Date deniedDate) {
		this.deniedDate = deniedDate;
	}

	public String getDeniedReason() {
		return this.deniedReason;
	}

	public void setDeniedReason(String deniedReason) {
		this.deniedReason = deniedReason;
	}

	public Timestamp getDeniedTime() {
		return this.deniedTime;
	}

	public void setDeniedTime(Timestamp deniedTime) {
		this.deniedTime = deniedTime;
	}

	public BigDecimal getIid() {
		return this.iid;
	}

	public void setIid(BigDecimal iid) {
		this.iid = iid;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPinPwd() {
		return this.pinPwd;
	}

	public void setPinPwd(String pinPwd) {
		this.pinPwd = pinPwd;
	}

	public String getReference() {
		return this.reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Timestamp getRequestTime() {
		return this.requestTime;
	}

	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}

	public String getSecurity1() {
		return this.security1;
	}

	public void setSecurity1(String security1) {
		this.security1 = security1;
	}

	public String getSecurity2() {
		return this.security2;
	}

	public void setSecurity2(String security2) {
		this.security2 = security2;
	}

	public String getSecurity3() {
		return this.security3;
	}

	public void setSecurity3(String security3) {
		this.security3 = security3;
	}

	public Date getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Timestamp getSendTime() {
		return this.sendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}

	public String getToAccount() {
		return this.toAccount;
	}

	public void setToAccount(String toAccount) {
		this.toAccount = toAccount;
	}

	public BigDecimal getToBank() {
		return this.toBank;
	}

	public void setToBank(BigDecimal toBank) {
		this.toBank = toBank;
	}

	public BigDecimal getToBranch() {
		return this.toBranch;
	}

	public void setToBranch(BigDecimal toBranch) {
		this.toBranch = toBranch;
	}

	public String getTrxNum() {
		return this.trxNum;
	}

	public void setTrxNum(String trxNum) {
		this.trxNum = trxNum;
	}

	public String getTrxNum2() {
		return this.trxNum2;
	}

	public void setTrxNum2(String trxNum2) {
		this.trxNum2 = trxNum2;
	}

	public String getWebUser() {
		return this.webUser;
	}

	public void setWebUser(String webUser) {
		this.webUser = webUser;
	}

}