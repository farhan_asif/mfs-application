package com.entertech.mfsuser.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the ACCOUNT_DETAILS database table.
 * 
 */
@Entity
@Table(name="ACCOUNT_DETAILS")
@NamedQuery(name="AccountDetail.findAll", query="SELECT a FROM AccountDetail a")
public class AccountDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACCOUNT_DETAILS_IID_GENERATOR", sequenceName="ACCOUNT_DETAILS_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACCOUNT_DETAILS_IID_GENERATOR")
	private long iid;

	@Column(name="ACCESS_LIMIT_ENABLED")
	private BigDecimal accessLimitEnabled;

	@Column(name="ACCOUNT_HOLDER_NAME")
	private String accountHolderName;

	@Column(name="ACCOUNT_NUM")
	private String accountNum;

	@Column(name="BANK_ID")
	private BigDecimal bankId;

	@Column(name="BANK_NAME")
	private String bankName;

	@Column(name="BRANCH_ID")
	private BigDecimal branchId;

	@Column(name="BRANCH_NAME")
	private String branchName;

	@Column(name="BRANCH_OFFICE_NAME")
	private String branchOfficeName;

	@Column(name="CREARED_MODE")
	private String crearedMode;

	@Column(name="IS_APPROVED")
	private BigDecimal isApproved;

	@Temporal(TemporalType.DATE)
	@Column(name="OPEN_BFTN_DATE")
	private Date openBftnDate;

	@Temporal(TemporalType.DATE)
	@Column(name="OPEN_BRANCH_DATE")
	private Date openBranchDate;

	@Temporal(TemporalType.DATE)
	@Column(name="OPEN_DATE")
	private Date openDate;
	
	@Column(name="MOBILE_NO")
	private String mobileNo;

	@Column(name="PRIMARY_DEPOSITE")
	private BigDecimal primaryDeposite;

	@Column(name="USER_LIMIT_ENABLED")
	private BigDecimal userLimitEnabled;

	@Column(name="WEB_USER")
	private String webUser;

	//bi-directional many-to-one association to AccountInfo
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ACCOUNT_INFO_IID")
	private AccountInfo accountInfo;

	public AccountDetail() {
	}

	public long getIid() {
		return this.iid;
	}

	public void setIid(long iid) {
		this.iid = iid;
	}

	public BigDecimal getAccessLimitEnabled() {
		return this.accessLimitEnabled;
	}

	public void setAccessLimitEnabled(BigDecimal accessLimitEnabled) {
		this.accessLimitEnabled = accessLimitEnabled;
	}

	public String getAccountHolderName() {
		return this.accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountNum() {
		return this.accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public BigDecimal getBankId() {
		return this.bankId;
	}

	public void setBankId(BigDecimal bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public BigDecimal getBranchId() {
		return this.branchId;
	}

	public void setBranchId(BigDecimal branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchOfficeName() {
		return this.branchOfficeName;
	}

	public void setBranchOfficeName(String branchOfficeName) {
		this.branchOfficeName = branchOfficeName;
	}

	public String getCrearedMode() {
		return this.crearedMode;
	}

	public void setCrearedMode(String crearedMode) {
		this.crearedMode = crearedMode;
	}

	public BigDecimal getIsApproved() {
		return this.isApproved;
	}

	public void setIsApproved(BigDecimal isApproved) {
		this.isApproved = isApproved;
	}

	public Date getOpenBftnDate() {
		return this.openBftnDate;
	}

	public void setOpenBftnDate(Date openBftnDate) {
		this.openBftnDate = openBftnDate;
	}

	public Date getOpenBranchDate() {
		return this.openBranchDate;
	}

	public void setOpenBranchDate(Date openBranchDate) {
		this.openBranchDate = openBranchDate;
	}

	public Date getOpenDate() {
		return this.openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public BigDecimal getPrimaryDeposite() {
		return this.primaryDeposite;
	}

	public void setPrimaryDeposite(BigDecimal primaryDeposite) {
		this.primaryDeposite = primaryDeposite;
	}

	public BigDecimal getUserLimitEnabled() {
		return this.userLimitEnabled;
	}

	public void setUserLimitEnabled(BigDecimal userLimitEnabled) {
		this.userLimitEnabled = userLimitEnabled;
	}

	public String getWebUser() {
		return this.webUser;
	}

	public void setWebUser(String webUser) {
		this.webUser = webUser;
	}

	public AccountInfo getAccountInfo() {
		return this.accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

}