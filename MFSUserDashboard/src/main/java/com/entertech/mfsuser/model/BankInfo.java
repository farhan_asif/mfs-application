package com.entertech.mfsuser.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BANK_INFO database table.
 * 
 */
@Entity
@Table(name="BANK_INFO")
@NamedQuery(name="BankInfo.findAll", query="SELECT b FROM BankInfo b")
public class BankInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BANK_INFO_BANKID_GENERATOR", sequenceName="BANK_INFO_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BANK_INFO_BANKID_GENERATOR")
	@Column(name="BANK_ID")
	private long bankId;

	@Column(name="BANK_NAME")
	private String bankName;

	@Column(name="ENTERED_BY")
	private String enteredBy;

	@Temporal(TemporalType.DATE)
	@Column(name="ENTERED_DATE")
	private Date enteredDate;

	@Column(name="LETTER_REF")
	private String letterRef;

	@Column(name="SERVICE_URL")
	private String serviceUrl;

	public BankInfo() {
	}

	public long getBankId() {
		return this.bankId;
	}

	public void setBankId(long bankId) {
		this.bankId = bankId;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getEnteredBy() {
		return this.enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredDate() {
		return this.enteredDate;
	}

	public void setEnteredDate(Date enteredDate) {
		this.enteredDate = enteredDate;
	}

	public String getLetterRef() {
		return this.letterRef;
	}

	public void setLetterRef(String letterRef) {
		this.letterRef = letterRef;
	}

	public String getServiceUrl() {
		return this.serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

}