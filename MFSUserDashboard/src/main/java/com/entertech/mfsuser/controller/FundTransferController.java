package com.entertech.mfsuser.controller;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.entertech.mfsuser.model.AccountDetail;
import com.entertech.mfsuser.model.AccountInfo;
import com.entertech.mfsuser.model.GetFund;
import com.entertech.mfsuser.model.SendFund;
import com.entertech.mfsuser.service.AccountDetailService;
import com.entertech.mfsuser.service.AccountInfoService;
import com.entertech.mfsuser.service.FundTransferService;

@Controller
@SessionAttributes("roles")
public class FundTransferController {

	@Autowired
	FundTransferService fundTransferService;
	
	@Autowired
	AccountDetailService accountDetailService;
	
	@Autowired
	AccountInfoService accountInfoService;
	//@Autowired
	//BankInfoService bankInfoService;
	
	//@Autowired
	//BranchInfoService branchInfoService;
	
	@RequestMapping(value = {"/getfund" }, method = RequestMethod.GET)
	public String getFund(ModelMap model) {
		String username=getPrincipal();
		AccountInfo accountInfo=accountInfoService.findByUserId(username);
		GetFund getFund=new GetFund();
		List<AccountDetail> accounts = accountDetailService.findByMobileNo(accountInfo.getMobileNo());
		model.addAttribute("accounts", accounts);
		model.addAttribute("getFund", getFund);
		return "getfund";
	}
	
	@RequestMapping(value = {"/getfund" }, method = RequestMethod.POST)
	public String requestGetFund(GetFund getFund, BindingResult result, ModelMap model) {
		AccountDetail accountDetail=accountDetailService.findByAccountNum(getFund.getAccountNum());
		//total 7 parameter 2 comes from form
		getFund.setTrxNum(getTrxNumber());
		getFund.setBankId(accountDetail.getBankId());
		getFund.setBranchId(accountDetail.getBranchId());
		getFund.setWebUser(accountDetail.getWebUser());
		getFund.setMobileNo(accountDetail.getMobileNo());
		
		fundTransferService.getFundRequest(getFund);
		return "redirect:success";
	}
	
	@RequestMapping(value = {"/sendfund" }, method = RequestMethod.GET)
	public String sendFund(ModelMap model) {
		String username=getPrincipal();
		AccountInfo accountInfo=accountInfoService.findByUserId(username);
		SendFund sendFund=new SendFund();
		List<AccountDetail> accounts = accountDetailService.findByMobileNo(accountInfo.getMobileNo());
		model.addAttribute("accounts", accounts);
		model.addAttribute("sendFund", sendFund);
		return "sendfund";
	}
	
	@RequestMapping(value = {"/sendfund" }, method = RequestMethod.POST)
	public String requestSendFund(SendFund sendFund, BindingResult result, ModelMap model) {
		sendFund.setTrxNum(getTrxNumber());
		
		//get account details of Beneficiary account details by account number
		AccountDetail toAccountDetail=accountDetailService.findByAccountNum(sendFund.getToAccount());
		sendFund.setToBank(toAccountDetail.getBankId());
		sendFund.setToBranch(toAccountDetail.getBranchId());
		String toWebUser=toAccountDetail.getWebUser();
		
		//Dabident Account holder information(From Account)
		AccountDetail fromAccountDetail=accountDetailService.findByAccountNum(sendFund.getAccountNum());
		sendFund.setWebUser(fromAccountDetail.getWebUser());
		sendFund.setBankId(fromAccountDetail.getBankId());
		sendFund.setBranchId(fromAccountDetail.getBranchId());
		sendFund.setMobileNo(fromAccountDetail.getMobileNo());
		
		
		fundTransferService.sendFundRequest(sendFund, toWebUser);
		return "redirect:success";
	}
	
	private static String getTrxNumber()
	{
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 20) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
	}
	
	/**
     * This method returns the principal[mobileNo] of logged-in user.
     */
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}
