package com.entertech.mfsuser.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.entertech.mfsuser.model.AccountDetail;
import com.entertech.mfsuser.model.AccountInfo;
import com.entertech.mfsuser.model.BankInfo;
import com.entertech.mfsuser.model.BranchInfo;
import com.entertech.mfsuser.service.AccountDetailService;
import com.entertech.mfsuser.service.AccountInfoService;
import com.entertech.mfsuser.service.BankInfoService;
import com.entertech.mfsuser.service.BranchInfoService;
import com.entertech.mfsuser.volt.PasswordGenerator;


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

	@Autowired
	AccountInfoService accountInfoService;
	
	@Autowired
	AccountDetailService accountDetailService;
	
	@Autowired
	BankInfoService bankInfoService;
	
	@Autowired
	BranchInfoService branchInfoService;
	
	@Autowired
	MessageSource messageSource;
	
	//@Autowired
    //AuthenticationTrustResolver authenticationTrustResolver;
	
	@RequestMapping(value = {"/accountopen" }, method = RequestMethod.GET)
	public String accountOpen(ModelMap model) {
		AccountDetail accountDetail = new AccountDetail();
		
		List<BankInfo> bankList=bankInfoService.findAllBanks();
		//TODO branch info shift to dynamic
		List<BranchInfo> branchList=branchInfoService.findByBankId(BigDecimal.valueOf(2));
		model.addAttribute("account", accountDetail);
		model.addAttribute("banks", bankList);
		model.addAttribute("branchs",branchList);
		return "accountopen";
	}
	
	@RequestMapping(value={"/saveaccount"}, method=RequestMethod.POST)
	public String accountSave(AccountDetail accountDetail, BindingResult result,
			ModelMap model) throws ParseException
	{
		Date dt=new Date();
		if (result.hasErrors()) {
			return "accountopen";
		}
		
		AccountInfo accountInfo = new AccountInfo();	
		String password=generateWebUserPassword();
		//TODO: Need to Encrypt User Name and Password before save DB
		/*Encryption enc=new Encryption();
		try {
			String s=enc.encrypt(password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		accountInfo.setMobilePassword(password);
		accountInfo.setWebPassword(password);
		accountInfo.setWebUser(String.valueOf(getWebUserId()));
		accountInfo.setMobileNo(accountDetail.getMobileNo());
		accountInfo.setOpenDate(dt);
		accountInfoService.saveAccountInfo(accountInfo);
		
		
		accountDetail.setAccountInfo(accountInfo);
		accountDetail.setWebUser(String.valueOf(getWebUserId()));
		accountDetail.setOpenDate(dt);
		accountDetail.setOpenBranchDate(dt);
		accountDetail.setOpenBftnDate(dt);
		accountDetail.setUserLimitEnabled(new BigDecimal(0));
		accountDetail.setIsApproved(new BigDecimal(1));
		accountDetail.setCrearedMode("WEB");
		//accountDetail.setBankName(bankInfoService.findBankNameById(accountDetail.getBankId()));
		//accountDetail.setBranchName(branchInfoService.findBranchNameById(accountDetail.getBranchId()));
		accountDetailService.save(accountDetail);
		
		return "redirect:success";
	}
	
	@RequestMapping(value = {"/addnewbankaccount" }, method = RequestMethod.GET)
	public String addNewBankAccount(ModelMap model) {
		String username=getPrincipal();
		AccountInfo accountInfo=accountInfoService.findByUserId(username);
		
		AccountDetail accountDetail = new AccountDetail();
		
		accountDetail.setMobileNo(accountInfo.getMobileNo());
		accountDetail.setAccountInfo(accountInfo);
		
		List<BankInfo> bankList=bankInfoService.findAllBanks();
		//TODO branch info shift to dynamic
		List<BranchInfo> branchList=branchInfoService.findByBankId(BigDecimal.valueOf(2));
		model.addAttribute("account", accountDetail);
		model.addAttribute("banks", bankList);
		model.addAttribute("branchs",branchList);
		return "addnewbankaccount";
	}
	
	@RequestMapping(value={"/savenewbankaccount"}, method=RequestMethod.POST)
	public String saveNewBankAccount(AccountDetail accountDetail, BindingResult result,
			ModelMap model) throws ParseException
	{
		Date dt=new Date();
		if (result.hasErrors()) {
			return "addnewbankaccount";
		}
		
		//accountDetail.setAccountInfo(accountInfo);
		accountDetail.setWebUser(String.valueOf(getWebUserId()));
		accountDetail.setOpenDate(dt);
		accountDetail.setOpenBranchDate(dt);
		accountDetail.setOpenBftnDate(dt);
		accountDetail.setUserLimitEnabled(new BigDecimal(0));
		accountDetail.setIsApproved(new BigDecimal(1));
		accountDetail.setCrearedMode("WEB");
		//accountDetail.setBankName(bankInfoService.findBankNameById(accountDetail.getBankId()));
		//accountDetail.setBranchName(branchInfoService.findBranchNameById(accountDetail.getBranchId()));
		accountDetailService.save(accountDetail);
		
		return "redirect:success";
	}
	
	
	@RequestMapping(value = {"/success" }, method = RequestMethod.GET)
	public String success(ModelMap model) {
		
		model.addAttribute("success","successfully complete your job");
		return "success";
	}
	
	/**
	 * This method will list all existing Account Holder.
	 */
	@RequestMapping(value = {"/accountlist" }, method = RequestMethod.GET)
	public String listAccounts(ModelMap model) {

		List<AccountDetail> accounts = accountDetailService.findAllAccountDetail();
		model.addAttribute("accounts", accounts);
		return "accountlist";
	}
	
	@RequestMapping(value = {"/passwordreset" }, method = RequestMethod.GET)
	public String passwordReset(ModelMap model) {
		AccountInfo accountInfo = new AccountInfo();
		model.addAttribute("account", accountInfo);
		return "passwordreset";
	}
	
	@RequestMapping(value = {"/passwordreset" }, method = RequestMethod.POST)
	public String savePassword(AccountInfo accountInfo, BindingResult result,ModelMap model) {
		//TODO: need to set primary key for update
		accountInfoService.updateAccountInfo(accountInfo);
		model.addAttribute("success","Successfully Changed Password.");
		return "success";
	}
	
	/**
     * This method handles login GET requests.
     * If users is already logged-in and tries to goto login page again, will be redirected to list page.
     */
    @RequestMapping(value = {"/","/login"}, method = RequestMethod.GET)
    public String loginPage(ModelMap model) {
    	//System.out.println(StringUtils.isEmpty(isCurrentAuthenticationAnonymous()));
    	//System.out.println(isCurrentAuthenticationAnonymous());
    	
        if (isCurrentAuthenticationAnonymous()) {
        	AccountInfo accountInfo = new AccountInfo();
    		model.addAttribute("account", accountInfo);
            return "login";
        } else {
            return "redirect:/getfund";  
        }
    }
    
    /**
     * This method handles logout requests.
     * Toggle the handlers if you are RememberMe functionality is useless in your app.
     */
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){    
            //new SecurityContextLogoutHandler().logout(request, response, auth);
            //persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }
	
	
	private static int getWebUserId() {
		//System.out.println("Boom");
		int max=9999999,min=100000;
		Random r = new Random();
		return r.nextInt((max - min) + 1) +min;
	}
	
	private static String generateWebUserPassword()
	{
		PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
		        .useDigits(true)
		        .useLower(true)
		        .useUpper(true)
		        .build();
		
		return passwordGenerator.generate(8);
	}
	
	/**
     * This method returns the principal[user-name] of logged-in user.
     */
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
	
	/**
     * This method returns true if users is already authenticated [logged-in], else false.
     */
    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AuthenticationTrustResolver authenticationTrustResolver = new AuthenticationTrustResolverImpl();
        return authenticationTrustResolver.isAnonymous(authentication);
        //return authenticationTrustResolver.isAnonymous(authentication);
    }
 
	
}
