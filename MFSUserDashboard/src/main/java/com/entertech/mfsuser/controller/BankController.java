package com.entertech.mfsuser.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entertech.mfsuser.model.BankInfo;
import com.entertech.mfsuser.model.BranchInfo;
import com.entertech.mfsuser.service.BankInfoService;
import com.entertech.mfsuser.service.BranchInfoService;

@RestController
public class BankController {
	
	@Autowired
	BankInfoService bankInfoService;
	
	@Autowired
	BranchInfoService branchInfoService;
	
	@RequestMapping(value ="/bank", method=RequestMethod.GET,produces = { "application/json" })
    public List<BankInfo> findAllBanks() {
        return bankInfoService.findAllBanks();
    }
	
	@RequestMapping(value ="/branch/{bankid}", method=RequestMethod.GET, produces = { "application/json" })
    public List<BranchInfo> findBranchByBankId(@PathVariable("bankid") BigDecimal bankId) {
        return branchInfoService.findByBankId(bankId);
    }
}
