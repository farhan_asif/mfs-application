package com.entertech.mfsuser.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entertech.mfsuser.model.AccountInfo;
import com.entertech.mfsuser.service.AccountInfoService;

@RestController
public class AccountController {
	
	
	@Autowired
	AccountInfoService accountInfoService;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value ="/aco", method=RequestMethod.POST,consumes="application/json",produces="application/json")//Account Open
	public ResponseEntity openAccount(@RequestBody AccountInfo accountInfo)//, @RequestBody AccountDetail accountDetail
	{
		
		accountInfoService.saveAccountInfo(accountInfo);
		return null;
	}
}
