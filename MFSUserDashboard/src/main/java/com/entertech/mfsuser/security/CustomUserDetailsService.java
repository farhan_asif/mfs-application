package com.entertech.mfsuser.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import com.entertech.mfsuser.model.AccountDetail;
import com.entertech.mfsuser.model.AccountInfo;
import com.entertech.mfsuser.service.AccountInfoService;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{

	static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	@Autowired
	private AccountInfoService accountInfoService;
	
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String webUser)
			throws UsernameNotFoundException {
		AccountInfo account = accountInfoService.findByUserId(webUser);
		
		logger.info("AccountInfo : {}", account);
		if(account==null){
			System.out.println("account not found");
			logger.info("account not found");
			throw new UsernameNotFoundException("Username not found");
		}
			return new org.springframework.security.core.userdetails.User(account.getWebUser(), account.getWebPassword(), 
				 true, true, true, true, getGrantedAuthorities(account));
	}

	
	private List<GrantedAuthority> getGrantedAuthorities(AccountInfo account){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		
		/*for(AccountDetail accountDetails : account.getAccountDetails()){
			logger.info("accountDetails : {}", accountDetails);
			//authorities.add(new SimpleGrantedAuthority("ROLE_"+userProfile.getType()));
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		}*/
		logger.info("authorities : {}", authorities);
		return authorities;
	}
	
}
