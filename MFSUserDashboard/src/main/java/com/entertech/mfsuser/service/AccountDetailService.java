package com.entertech.mfsuser.service;

import java.util.List;

import com.entertech.mfsuser.model.AccountDetail;

public interface AccountDetailService {
	AccountDetail findById(int id);
	void save(AccountDetail accountDetail);
	List<AccountDetail> findAllAccountDetail();
	List<AccountDetail> findByMobileNo(String mobileNo);
	AccountDetail findByAccountNum(String accountNum);
}
