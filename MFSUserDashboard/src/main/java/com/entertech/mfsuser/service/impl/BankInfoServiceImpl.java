package com.entertech.mfsuser.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entertech.mfsuser.dao.BankInfoDao;
import com.entertech.mfsuser.model.BankInfo;
import com.entertech.mfsuser.service.BankInfoService;

@Service("bankInfoService")
@Transactional
public class BankInfoServiceImpl implements BankInfoService {
	
	@Autowired
	private BankInfoDao dao;

	@Override
	public BankInfo findById(int id) {
		return dao.findById(id);
	}

	@Override
	public List<BankInfo> findAllBanks() {
		return dao.findAllBanks();
	}

	@Override
	public String findBankNameById(BigDecimal id) {
		// TODO Auto-generated method stub
		return dao.findBankNameById(id);
	}

}
