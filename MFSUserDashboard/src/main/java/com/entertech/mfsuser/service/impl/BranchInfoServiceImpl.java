package com.entertech.mfsuser.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entertech.mfsuser.dao.BranchInfoDao;
import com.entertech.mfsuser.model.BranchInfo;
import com.entertech.mfsuser.service.BranchInfoService;

@Service("branchInfoService")
@Transactional
public class BranchInfoServiceImpl implements BranchInfoService {

	@Autowired
	private BranchInfoDao dao;
	
	@Override
	public List<BranchInfo> findAll() {
		
		return dao.findAll();
	}

	@Override
	public BranchInfo findById(int id) {
		
		return dao.findById(id);
	}

	@Override
	public List<BranchInfo> findByBankId(BigDecimal bankId) {
		//System.out.println("I am In Service");
		return dao.findByBankId(bankId);
	}

	@Override
	public String findBranchNameById(BigDecimal id) {
		// TODO Auto-generated method stub
		return dao.findBranchNameById(id);
	}

}
