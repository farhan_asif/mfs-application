package com.entertech.mfsuser.service;

import java.math.BigDecimal;
import java.util.List;

import com.entertech.mfsuser.model.BankInfo;

public interface BankInfoService {
	
	BankInfo findById(int id);
	List<BankInfo> findAllBanks();
	String findBankNameById(BigDecimal id);
}
