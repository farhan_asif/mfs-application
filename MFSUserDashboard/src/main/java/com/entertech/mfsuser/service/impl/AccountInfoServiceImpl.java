package com.entertech.mfsuser.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entertech.mfsuser.dao.AccountInfoDao;
import com.entertech.mfsuser.model.AccountInfo;
import com.entertech.mfsuser.service.AccountInfoService;

@Service("accountInfoService")
@Transactional
public class AccountInfoServiceImpl implements AccountInfoService {

	@Autowired
	private AccountInfoDao dao;
	
	@Override
	public AccountInfo findById(int iid) {
		return dao.findById(iid);
	}

	@Override
	public void saveAccountInfo(AccountInfo accountInfo) {
		dao.save(accountInfo);
	}

	@Override
	public void updateAccountInfo(AccountInfo accountInfo) {
		// TODO Auto-generated method stub
		dao.updateAccountInfo(accountInfo);
	}

	@Override
	public List<AccountInfo> findAllAccountInfo() {
		return dao.findAllAccountInfo();
	}

	@Override
	public AccountInfo findByMobileNo(String mobileNo) {
		// TODO Auto-generated method stub
		return dao.findByMobileNo(mobileNo);
	}

	@Override
	public AccountInfo findByUserId(String webUser) {
		// TODO Auto-generated method stub
		return dao.findByUserId(webUser);
	}

}
