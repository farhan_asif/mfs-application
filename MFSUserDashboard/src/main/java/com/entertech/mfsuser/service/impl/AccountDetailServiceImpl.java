package com.entertech.mfsuser.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entertech.mfsuser.dao.AccountDetailDao;
import com.entertech.mfsuser.model.AccountDetail;
import com.entertech.mfsuser.service.AccountDetailService;

@Service("accountDetailService")
@Transactional
public class AccountDetailServiceImpl implements AccountDetailService {

	@Autowired
	private AccountDetailDao dao;
	
	@Override
	public AccountDetail findById(int id) {
		return dao.findById(id);
	}

	@Override
	public void save(AccountDetail accountDetail) {
		dao.save(accountDetail);
	}

	@Override
	public List<AccountDetail> findAllAccountDetail() {
		return dao.findAllAccountDetail();
	}

	@Override
	public List<AccountDetail> findByMobileNo(String mobileNo) {
		return dao.findByMobileNo(mobileNo);
	}

	@Override
	public AccountDetail findByAccountNum(String accountNum) {
		// TODO Auto-generated method stub
		return dao.findByAccountNum(accountNum);
	}

}
