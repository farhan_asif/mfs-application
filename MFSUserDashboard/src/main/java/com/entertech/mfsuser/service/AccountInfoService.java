package com.entertech.mfsuser.service;

import java.util.List;

import com.entertech.mfsuser.model.AccountInfo;

public interface AccountInfoService {

	AccountInfo findById(int iid);
	AccountInfo findByUserId(String webUser);
	void saveAccountInfo(AccountInfo accountInfo);
	void updateAccountInfo(AccountInfo accountInfo);
	List<AccountInfo> findAllAccountInfo(); 
	AccountInfo findByMobileNo(String mobileNo);
}
