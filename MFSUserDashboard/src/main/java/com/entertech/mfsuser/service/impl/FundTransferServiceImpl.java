package com.entertech.mfsuser.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entertech.mfsuser.dao.FundTransferDao;
import com.entertech.mfsuser.model.GetFund;
import com.entertech.mfsuser.model.SendFund;
import com.entertech.mfsuser.service.FundTransferService;

@Service("fundTransferService")
@Transactional
public class FundTransferServiceImpl implements FundTransferService {

	@Autowired
	private FundTransferDao dao;
	
	@Override
	public String getFundRequest(GetFund getFund) {
		return dao.getFundRequest(getFund);
	}

	@Override
	public String sendFundRequest(SendFund sendFund, String toWebUser) {
		// TODO Auto-generated method stub
		return dao.sendFundRequest(sendFund, toWebUser);
	}

}
