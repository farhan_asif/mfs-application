package com.entertech.mfsuser.service;

import com.entertech.mfsuser.model.GetFund;
import com.entertech.mfsuser.model.SendFund;

public interface FundTransferService {

	String getFundRequest(GetFund getFund);
	String sendFundRequest(SendFund sendFund,String toWebUser);
}
