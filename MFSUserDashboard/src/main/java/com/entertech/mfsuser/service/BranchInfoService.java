package com.entertech.mfsuser.service;

import java.math.BigDecimal;
import java.util.List;

import com.entertech.mfsuser.model.BranchInfo;

public interface BranchInfoService {
	List<BranchInfo> findAll();
	BranchInfo findById(int id);
	List<BranchInfo> findByBankId(BigDecimal bankId);
	String findBranchNameById(BigDecimal id);
}
