package com.entertech.mfsuser.volt;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@PropertySource(value = { "classpath:application.properties" })
public final class Encryption {
	
	@Autowired
	private static Environment environment;
	
	private static byte[] keyValue=environment.getProperty("mfs.key", byte[].class);
	private static String ALGORITHM=environment.getProperty("mfs.algorithm");
	
	public static String encrypt(String Data) throws Exception {
	    Key key = generateKey();
	    Cipher c = Cipher.getInstance(ALGORITHM);
	    c.init(Cipher.ENCRYPT_MODE, key);
	    byte[] encVal = c.doFinal(Data.getBytes());
	    String encryptedValue = Base64.getEncoder().encodeToString(encVal);
	    return encryptedValue;
	}
	
	private static Key generateKey() throws Exception {
	    Key key = new SecretKeySpec(keyValue, ALGORITHM);
	    return key;
	}

}
