package com.entertech.mfsuser.dao.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;
import com.entertech.mfsuser.dao.AbstractDao;
import com.entertech.mfsuser.dao.BankInfoDao;
import com.entertech.mfsuser.model.BankInfo;

@Repository("bankInfoDao")
public class BankInfoDaoImpl extends AbstractDao<Integer, BankInfo> implements BankInfoDao {

	@Override
	public BankInfo findById(int id) {
		BankInfo bankInfo = getByKey(id);
		
		return bankInfo;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<BankInfo> findAllBanks() {
		
		List<BankInfo> bankInfos= getEntityManager()
				.createQuery("SELECT b FROM BankInfo b")
				.getResultList();
		
		return bankInfos;
	}
	
	//An alternative to Hibernate.initialize()
		protected void initializeCollection(Collection<?> collection) {
		    if(collection == null) {
		        return;
		    }
		    collection.iterator().hasNext();
		}


	@Override
	public String findBankNameById(BigDecimal id) {
		String bankName=getEntityManager()
				.createQuery("select b.bankName from BankInfo b where b.bankId = :bankId")
				.setParameter("bankId", id).toString();
		
		return bankName;
	}


		
}
