package com.entertech.mfsuser.dao;

import java.math.BigDecimal;
import java.util.List;

import com.entertech.mfsuser.model.BranchInfo;

public interface BranchInfoDao {

	List<BranchInfo> findAll();
	BranchInfo findById(int id);
	List<BranchInfo> findByBankId(BigDecimal bankId);
	String findBranchNameById(BigDecimal id);
}
