package com.entertech.mfsuser.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Repository;
import com.entertech.mfsuser.dao.AbstractDao;
import com.entertech.mfsuser.dao.BranchInfoDao;
import com.entertech.mfsuser.model.BranchInfo;

@Repository("branchInfoDao")
public class BranchInfoDaoImpl extends AbstractDao<Integer, BranchInfo> implements BranchInfoDao {

	
	@SuppressWarnings("unchecked")
	@Override
	public List<BranchInfo> findAll() {
		List<BranchInfo> branchInfos=getEntityManager().createQuery("select br from BranchInfo br").getResultList();
		return branchInfos;
	}

	@Override
	public BranchInfo findById(int id) {
		return getByKey(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BranchInfo> findByBankId(BigDecimal bankId) {
		

		/*List<BankInfo> bankInfos=getEntityManager()
				.createQuery("select br from bank_info br where br.bankId = :bankId")
				.setParameter(bankId, bankId)
				.getResultList();*/
		//System.out.println(bankId);
		
		List<BranchInfo> branchInfos=getEntityManager()
				.createQuery("select br from BranchInfo br where br.bankId = :bankId")
				.setParameter("bankId", bankId)
				.getResultList();
		
		
		return branchInfos;
	}

	@Override
	public String findBranchNameById(BigDecimal id) {
		// TODO Auto-generated method stub
		String branchName=getEntityManager()
				.createQuery("select br.branchName from BranchInfo br where br.branchId = :id")
				.setParameter("id", id).toString();
		
		return branchName;
	}

}
