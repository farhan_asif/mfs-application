package com.entertech.mfsuser.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.entertech.mfsuser.dao.AbstractDao;
import com.entertech.mfsuser.dao.AccountInfoDao;
import com.entertech.mfsuser.model.AccountDetail;
import com.entertech.mfsuser.model.AccountInfo;

@Transactional
@Repository("accountInfoDao")
public class AccountInfoDaoImpl extends AbstractDao<Integer, AccountInfo> implements AccountInfoDao {

	@Override
	public AccountInfo findById(int id) {
		AccountInfo accountInfo = getByKey(id);
		/*if(accountInfo!=null){
			initializeCollection(accountInfo..getAccountDetails());
		}*/
		return accountInfo;
	}

	@Override
	public void save(AccountInfo accountInfo) {
		//ToDo: Need to investigate why persist(entity obj) not workng
		//getEntityManager().merge(accountInfo);
		persist(accountInfo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccountInfo> findAllAccountInfo() {
		List<AccountInfo> accountInfos = getEntityManager()
				.createQuery("SELECT ai FROM AccountInfo ai")
				.getResultList();
		return accountInfos;
	}

	@Override
	public AccountInfo findByMobileNo(String mobileNo) {
		AccountInfo accountInfo=(AccountInfo) getEntityManager()
				.createQuery("select ai from AccountInfo ai where ai.mobileNo = :mobileNo")
				.setParameter("mobileNo", mobileNo).getSingleResult();
		
		return accountInfo;
	}

	@Override
	public void updateAccountInfo(AccountInfo accountInfo) {
		// TODO Auto-generated method stub
		update(accountInfo);
	}

	@Override
	public AccountInfo findByUserId(String webUser) {
		AccountInfo accountInfo=(AccountInfo) getEntityManager()
				.createQuery("select ai from AccountInfo ai where ai.webUser = :webUser")
				.setParameter("webUser", webUser).getSingleResult();
		
		return accountInfo;
	}
	
	/*//An alternative to Hibernate.initialize()
		protected void initializeCollection(Collection<?> collection) {
		    if(collection == null) {
		        return;
		    }
		    collection.iterator().hasNext();
		}*/

}
