package com.entertech.mfsuser.dao;

import java.util.List;

import com.entertech.mfsuser.model.AccountDetail;

public interface AccountDetailDao {
	AccountDetail findById(int id);
	void save(AccountDetail accountDetail);
	List<AccountDetail> findAllAccountDetail();
	List<AccountDetail> findByMobileNo(String mobileNo);
	AccountDetail findByAccountNum(String accountNum);
}
