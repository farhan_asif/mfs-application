package com.entertech.mfsuser.dao;

import com.entertech.mfsuser.model.GetFund;
import com.entertech.mfsuser.model.SendFund;

public interface FundTransferDao {

	String getFundRequest(GetFund getFund);
	String sendFundRequest(SendFund sendFund,String toWebUser);
}
