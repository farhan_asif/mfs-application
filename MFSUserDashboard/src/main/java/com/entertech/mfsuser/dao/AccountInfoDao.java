package com.entertech.mfsuser.dao;

import java.util.List;

import com.entertech.mfsuser.model.AccountInfo;

public interface AccountInfoDao {

	AccountInfo findById(int id);
	AccountInfo findByUserId(String webUser);
	void save(AccountInfo accountInfo);
	void updateAccountInfo(AccountInfo accountInfo);
	List<AccountInfo> findAllAccountInfo();
	AccountInfo findByMobileNo(String mobileNo);
}
