package com.entertech.mfsuser.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.entertech.mfsuser.dao.AbstractDao;
import com.entertech.mfsuser.dao.AccountDetailDao;
import com.entertech.mfsuser.model.AccountDetail;

@Repository("accountDetailDao")
public class AccountDetailDaoImpl extends AbstractDao<Integer, AccountDetail> implements AccountDetailDao {

	@Override
	public AccountDetail findById(int id) {
		AccountDetail accountDetail = getByKey(id);
		return accountDetail;
	}

	@Override
	public void save(AccountDetail accountDetail) {
		update(accountDetail);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccountDetail> findAllAccountDetail() {
		List<AccountDetail> accountDetails = getEntityManager()
				.createQuery("SELECT ad FROM AccountDetail ad")
				.getResultList();
		return accountDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AccountDetail> findByMobileNo(String mobileNo) {
		List<AccountDetail> accountDetails=getEntityManager()
				.createQuery("select ad from AccountDetail ad where ad.mobileNo = :mobileNo")
				.setParameter("mobileNo", mobileNo)
				.getResultList();
		
		return accountDetails;
	}

	@Override
	public AccountDetail findByAccountNum(String accountNum) {
		AccountDetail accountDetail=(AccountDetail) getEntityManager()
				.createQuery("select ad from AccountDetail ad where ad.accountNum = :accountNum")
				.setParameter("accountNum", accountNum).getSingleResult();
		
		return accountDetail;
	}

}
