package com.entertech.mfsuser.dao;

import java.math.BigDecimal;
import java.util.List;

import com.entertech.mfsuser.model.BankInfo;

public interface BankInfoDao {

	BankInfo findById(int id);
	List<BankInfo> findAllBanks();
	String findBankNameById(BigDecimal id);
}
