package com.entertech.mfsuser.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Repository;

import com.entertech.mfsuser.dao.FundTransferDao;
import com.entertech.mfsuser.model.GetFund;
import com.entertech.mfsuser.model.SendFund;

@Repository("fundTransferDao")
public class FundTransferDaoImpl implements FundTransferDao {

	@PersistenceContext
	EntityManager entityManager;
	
	protected EntityManager getEntityManager(){
		return this.entityManager;
	}

	@Override
	public String getFundRequest(GetFund getFund) {
		// TODO Auto-generated method stub
		
		StoredProcedureQuery storedProcedure=getEntityManager().createStoredProcedureQuery("SP_GET_FUND_REQUEST");
		storedProcedure.registerStoredProcedureParameter("AccountNum", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("MobileNo", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("WebUser", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("TotalAmount", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("BankId", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("BranchId", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("TrxNum", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("OUT_MESSAGE", String.class, ParameterMode.OUT);
		
		
		//System.out.println();
		storedProcedure.setParameter("AccountNum",getFund.getAccountNum());
		storedProcedure.setParameter("MobileNo",getFund.getMobileNo());
		storedProcedure.setParameter("WebUser",getFund.getWebUser());
		storedProcedure.setParameter("TotalAmount",(int)getFund.getAmount());
		storedProcedure.setParameter("BankId",getFund.getBankId().intValue());
		storedProcedure.setParameter("BranchId",getFund.getBranchId().intValue());
		storedProcedure.setParameter("TrxNum",getFund.getTrxNum());
		
		storedProcedure.execute();
		return storedProcedure.getOutputParameterValue("OUT_MESSAGE").toString();
	}

	@Override
	public String sendFundRequest(SendFund sendFund,String toWebUser) {
		// TODO Auto-generated method stub
		
		StoredProcedureQuery storedProcedure=getEntityManager().createStoredProcedureQuery("SP_SEND_FUND_REQUEST");
		storedProcedure.registerStoredProcedureParameter("MobileNo", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("WebUser", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("AccountNum", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("BankId", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("BranchId", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("TotalAmount", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("To_BankId", Integer.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("To_BranchId", Integer.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("To_AccountNum", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("To_WebUser", String.class, ParameterMode.IN); 
		storedProcedure.registerStoredProcedureParameter("TrxNum", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("Out_Message", String.class, ParameterMode.OUT);
		
		storedProcedure.setParameter("MobileNo",sendFund.getMobileNo());
		storedProcedure.setParameter("WebUser",sendFund.getWebUser());
		storedProcedure.setParameter("AccountNum",sendFund.getAccountNum());
		storedProcedure.setParameter("BankId",sendFund.getBankId().intValue());
		storedProcedure.setParameter("BranchId",sendFund.getBranchId().intValue());
		storedProcedure.setParameter("TotalAmount",(int) sendFund.getAmount());
		storedProcedure.setParameter("To_BankId",sendFund.getToBank().intValue());
		storedProcedure.setParameter("To_BranchId",sendFund.getToBranch().intValue());
		storedProcedure.setParameter("To_AccountNum",sendFund.getToAccount());
		storedProcedure.setParameter("To_WebUser",toWebUser);
		storedProcedure.setParameter("TrxNum",sendFund.getTrxNum());
		
		storedProcedure.execute();
		return storedProcedure.getOutputParameterValue("Out_Message").toString();
	}

}
